package pl.sda.michalrys.sorts;

import java.util.Arrays;
import java.util.Random;

public class Main03InsertionSort {
    private static final int VALUES_HOW_MANY = 10;

    public static void main(String[] args) {
        // selection sort: https://visualgo.net/en/sorting
        int[] values = getRandomValues(VALUES_HOW_MANY, 100);
        //TEST int[] values = {2, 1, 3, -1};
        System.out.println(Arrays.toString(values));

        int valuePrevious;
        int jPrevious;
        int jTemp;
        int valueTemp;

        for (int i = 0; i < values.length; i++) {
            //TEST System.out.println("-- i = " + i);
            for (int j = i; j > 0; j--) {
                jTemp = j;
                valueTemp = values[jTemp];
                jPrevious = j - 1;
                valuePrevious = values[jPrevious];
                //TEST System.out.println(" - j = " + j + " vtemp = " + valueTemp + ", vprev = " + valuePrevious);
                if (valueTemp < valuePrevious) {
                    values[jPrevious] = valueTemp;
                    values[jTemp] = valuePrevious;
                    //TEST System.out.println("   OK replace values");
                } else {
                    //TEST System.out.println("   Do nothing");
                    break;
                }
                //TEST System.out.println(Arrays.toString(values));
            }
        }
        System.out.println("----- insertion sort -----");
        System.out.println(Arrays.toString(values));

    }

    private static int[] getRandomValues(int howMany, int endValue) {
        int[] values = new int[howMany];
        Random random = new Random();

        for (int i = 0; i < howMany; i++) {
            values[i] = random.nextInt(endValue) + 1;
        }
        return values;
    }
}
