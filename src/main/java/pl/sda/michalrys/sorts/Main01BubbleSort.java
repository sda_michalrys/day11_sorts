package pl.sda.michalrys.sorts;

import java.util.Arrays;
import java.util.Random;

public class Main01BubbleSort {
    public static void main(String[] args) {
        // bubble sort: https://visualgo.net/en/sorting

        Random random = new Random();
        int[] values = getRandomValues(random, 6, 50);
        //TEST int[] values = {40, 4, 3, 1};
        System.out.println(Arrays.toString(values));

        int valueCurrent;
        int valueNext;
        for (int i = 0; i < values.length - 1; i++) {
            for (int j = 1; j < values.length - i; j++) {
                valueCurrent = values[j - 1];
                valueNext = values[j];
                if (valueCurrent > valueNext) {
                    values[j - 1] = valueNext;
                    values[j] = valueCurrent;
                }
                //TEST System.out.println("i,j = " + i + "," + j + " : " + Arrays.toString(values));
            }
            //TEST System.out.println("--------------");
        }
        System.out.println(Arrays.toString(values));
    }

    private static int[] getRandomValues(Random random, int howMany, int MaxRandom) {
        int[] values = new int[howMany];
        for (int i = 0; i < values.length; i++) {
            values[i] = random.nextInt(MaxRandom) + 1;
        }
        return values;
    }
}
