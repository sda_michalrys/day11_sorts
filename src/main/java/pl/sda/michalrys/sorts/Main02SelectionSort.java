package pl.sda.michalrys.sorts;

import java.util.Arrays;
import java.util.Random;

public class Main02SelectionSort {
    private static final int VALUES_HOW_MANY = 5;

    public static void main(String[] args) {
        // selection sort: https://visualgo.net/en/sorting
        int[] values = getRandomValues(VALUES_HOW_MANY, 100);
        System.out.println(Arrays.toString(values));

        int valueCurrent;
        int valueLocalMin;
        int jLocalMin;
        for (int i = 0; i < values.length; i++) {
            valueLocalMin = values[i];
            jLocalMin = i;

            // find local minimum
            for (int j = i + 1; j < values.length; j++) {
                valueCurrent = values[j];
                if (valueCurrent < valueLocalMin) {
                    valueLocalMin = valueCurrent;
                    jLocalMin = j;
                }
            }

            // replace local minimum at jLocalMin with value at location i
            values[jLocalMin] = values[i];
            values[i] = valueLocalMin;
        }
        System.out.println("----- selection sort -----");
        System.out.println(Arrays.toString(values));
    }

    private static int[] getRandomValues(int howMany, int endValue) {
        int[] values = new int[howMany];
        Random random = new Random();

        for (int i = 0; i < howMany; i++) {
            values[i] = random.nextInt(endValue) + 1;
        }
        return values;
    }
}